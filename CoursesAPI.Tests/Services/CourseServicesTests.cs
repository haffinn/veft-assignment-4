﻿using System.Collections.Generic;
using System.Linq;
using CoursesAPI.Models;
using CoursesAPI.Services.Exceptions;
using CoursesAPI.Services.Models.Entities;
using CoursesAPI.Services.Services;
using CoursesAPI.Tests.MockObjects;
using CoursesAPI.Tests.TestExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
// ReSharper disable InconsistentNaming

namespace CoursesAPI.Tests.Services
{
    [TestClass]
	public class CourseServicesTests
	{
        // Mock unit of work and service provider
		private MockUnitOfWork<MockDataContext> _mockUnitOfWork;
        private CoursesServiceProvider _service;

        // Frequently used lists of initialized data
		private List<TeacherRegistration> _teacherRegistrations;
        private List<CourseInstance> _courseInstances;

        // Frequently used SSN's
		private const string SSN_DABS    = "1203735289";
		private const string SSN_GUNNA   = "1234567890";
		private const string INVALID_SSN = "9876543210";

        // Freq. used names
		private const string NAME_GUNNA  = "Guðrún Guðmundsdóttir";
        private const string NAME_DABS   = "Daníel Brandur Sigurgeirsson";

        // Frequently used Course ID's
		private const int COURSEID_VEFT_20153 = 1337;
		private const int COURSEID_VEFT_20163 = 1338;
        private const int COURSEID_PROG_20153 = 1336;
        private const int COURSEID_RUIS_20152 = 1335;
		private const int INVALID_COURSEID    = 9999;

		[TestInitialize]
		public void Setup()
		{
			_mockUnitOfWork = new MockUnitOfWork<MockDataContext>();

			#region Persons
			var persons = new List<Person>
			{
				// Of course I'm the first person,
				// did you expect anything else?
				new Person
				{
					ID    = 1,
					Name  = NAME_DABS,
					SSN   = SSN_DABS,
					Email = "dabs@ru.is"
				},
				new Person
				{
					ID    = 2,
					Name  = NAME_GUNNA,
					SSN   = SSN_GUNNA,
					Email = "gunna@ru.is"
				}
			};
			#endregion

			#region Course templates

			var courseTemplates = new List<CourseTemplate>
			{
				new CourseTemplate
				{
					CourseID    = "T-514-VEFT",
					Description = "Í þessum áfanga verður fjallað um vefþj...",
					Name        = "Vefþjónustur"
				},
                new CourseTemplate
                {
                    CourseID    = "T-111-PROG",
                    Description = "Geggjaður áfangi til að læra snilld og....",
                    Name        = "Forritun"
                },
                new CourseTemplate
                {
                    CourseID    = "T-426-RUIS",
                    Description = "Ef þú villt verða snillingur þá...",
                    Name        = "HR Starfsnám"
                }
                
			};
			#endregion

			#region Courses
			_courseInstances = new List<CourseInstance>
			{
				new CourseInstance
				{
					ID         = COURSEID_VEFT_20153,
					CourseID   = "T-514-VEFT",
					SemesterID = "20153"
				},
                new CourseInstance
                {
                    ID         = COURSEID_PROG_20153,
                    CourseID   = "T-111-PROG",
                    SemesterID = "20153"
                },
                new CourseInstance
                {
                    ID         = COURSEID_RUIS_20152,
                    CourseID   = "T-426-RUIS",
                    SemesterID = "20152"
                },
				new CourseInstance
				{
					ID         = COURSEID_VEFT_20163,
					CourseID   = "T-514-VEFT",
					SemesterID = "20163"
				}
			};
			#endregion

			#region Teacher registrations
			_teacherRegistrations = new List<TeacherRegistration>
			{
				new TeacherRegistration
				{
					ID               = 101,
					CourseInstanceID = COURSEID_VEFT_20153,
					SSN              = SSN_DABS,
					Type             = TeacherType.MainTeacher
				},
                new TeacherRegistration
                {
                    ID               = 102,
                    CourseInstanceID = COURSEID_RUIS_20152,
                    SSN              = SSN_DABS,
                    Type             = TeacherType.MainTeacher
                }
			};
			#endregion

			_mockUnitOfWork.SetRepositoryData(persons);
			_mockUnitOfWork.SetRepositoryData(courseTemplates);
			_mockUnitOfWork.SetRepositoryData(_courseInstances);
			_mockUnitOfWork.SetRepositoryData(_teacherRegistrations);

			// TODO: this would be the correct place to add 
			// more mock data to the mockUnitOfWork!

			_service = new CoursesServiceProvider(_mockUnitOfWork);
		}

        // ##############################################################################
        // GET COURSE INSTANCE/S TESTS:
        // ##############################################################################

		#region GetCoursesBySemester
		/// <summary>
		/// Checks that if no course is registered with the given semester,
		/// the function should return a empty list (no data).
		/// </summary>
	    [TestMethod]
		public void GetCoursesBySemester_ReturnsEmptyListWhenNoDataDefined()
		{
			// Arrange:
		    const string SEMESTER = "20151";
		    // var mockUnitOfWork = new MockUnitOfWork<MockDataContext>();
            // var service = new CoursesServiceProvider(mockUnitOfWork);

			// Act:
		    var result = _service.GetCourseInstancesBySemester(SEMESTER);

		    // Assert:
            Assert.AreEqual(0, result.Count, "The number of courses is incorrect");
		}

        /// <summary>
        /// Checks that if no semester is provided with request,
        /// the current semester will be used for the query
        /// </summary>
        [TestMethod]
        public void GetCoursesBySemester_returnsCurrentSemesterIfNoSemesterIsProvided()
        {
            // Arrange:
            const string CURRENT_SEMESTER = "20153";

            // Act:
            var dto = _service.GetCourseInstancesBySemester();


            // Assert
            // Check if semester value is equal to the current semester
            foreach (var c in dto)
            {
                Assert.AreEqual(CURRENT_SEMESTER, c.Semester);
            }

            // Make sure that we only return 3 instances (according to initialized test data)
            Assert.AreEqual(2, dto.Count, "The number of courses is incorrect");
        }

        /// <summary>
        /// Makes sure that exactly the correct number of course instances are returned for a given semester.
        /// This function tests for two semesters.
        /// </summary>
        [TestMethod]
        public void GetCoursesBySemester_returnsAllCoursesInASemesterNoMoreNoLess()
        {
            // Arrange:
            const string SEMESTER1 = "20153";
            const string SEMESTER2 = "20163";

            // Act:
            var dto1 = _service.GetCourseInstancesBySemester(SEMESTER1);
            var dto2 = _service.GetCourseInstancesBySemester(SEMESTER2);


            // Assert
            // Check if semester value is equal to the current semester
            foreach (var c in dto1)
            {
                Assert.AreEqual(SEMESTER1, c.Semester);
            }

            foreach (var c in dto2)
            {
                Assert.AreEqual(SEMESTER2, c.Semester);
            }

            // Make sure that we only return correct number 
            // ofinstances (according to initialized test data)
            Assert.AreEqual(2, dto1.Count, "The number of courses for 20153 is incorrect");
            Assert.AreEqual(1, dto2.Count, "The number of courses for 20163 is incorrect");

        }

        // For each course returned, the name of the main teacher 
        // of the course should be included (see the definition of CourseInstanceDTO).
        [TestMethod]
        public void GetCoursesBySemester_returnsCorrectMainTeacher()
        {
            // Arrange:
            const string SEMESTER = "20152";

            var course = new CourseInstanceDTO
            {
                CourseInstanceID = COURSEID_RUIS_20152,
                MainTeacher = NAME_DABS,
                Name = "HR Starfsnám",
                Semester = SEMESTER,
                TemplateID = "T-426-RUIS"
            };

            // Act:
            var dto = _service.GetCourseInstancesBySemester(SEMESTER);


            // Assert
            var entity = dto.First();
            
            Assert.AreEqual(1, dto.Count, "Wrong number of courses");

            // compare data from service to a predefined courseinstance above.
            Assert.AreEqual(course.MainTeacher, entity.MainTeacher);
            Assert.AreEqual(course.CourseInstanceID, entity.CourseInstanceID);
            Assert.AreEqual(course.Name, entity.Name);
            Assert.AreEqual(course.TemplateID, entity.TemplateID);
            Assert.AreEqual(course.Semester, entity.Semester);
        }

        /// <summary>
        /// Similar to correct teacher. Ensures that MainTeacher in Courseinstance will be
        /// an empty string if no main teacher is registered in the course.
        /// </summary>
        [TestMethod]
        public void GetCoursesBySemester_returnsEmptyStringIfNoTeacherIsRegistered()
        {
            // Arrange:
            const string SEMESTER = "20163";

            var course = new CourseInstanceDTO
            {
                CourseInstanceID = COURSEID_VEFT_20163,
                MainTeacher = "",
                Name = "Vefþjónustur",
                Semester = SEMESTER,
                TemplateID = "T-514-VEFT"
            };

            // Act:
            var dto = _service.GetCourseInstancesBySemester(SEMESTER);


            // Assert
            var entity = dto.First();

            Assert.AreEqual(1, dto.Count, "Wrong number of courses");

            // compare data from service to a predefined courseinstance above.
            Assert.AreEqual(course.MainTeacher, entity.MainTeacher);
            Assert.AreEqual(course.CourseInstanceID, entity.CourseInstanceID);
            Assert.AreEqual(course.Name, entity.Name);
            Assert.AreEqual(course.TemplateID, entity.TemplateID);
            Assert.AreEqual(course.Semester, entity.Semester);
        }

        [TestMethod]
        [ExpectedException(typeof(AppObjectNotFoundException))]
        public void GetMainTeacherOfCourse_throwsExceptionIfInvalidCourse()
        {
            // Act:
            var dto = _service.GetNameOfMainTeacherInCourse(INVALID_COURSEID);
        }

		#endregion


        // ##############################################################################
        // ADD TEACHER TESTS:
        // ##############################################################################
		#region AddTeacher

		/// <summary>
		/// Adds a main teacher to a course which doesn't have a
		/// main teacher defined already (see test data defined above).
		/// </summary>
		[TestMethod]
		public void AddTeacher_WithValidTeacherAndCourse()
		{
			// Arrange:
			var model = new AddTeacherViewModel
			{
				SSN  = SSN_GUNNA,
				Type = TeacherType.MainTeacher
			};
			var prevCount = _teacherRegistrations.Count;
			// Note: the method uses test data defined in [TestInitialize]

			// Act:
			var dto = _service.AddTeacherToCourse(COURSEID_VEFT_20163, model);

			// Assert:

			// Check that the dto object is correctly populated:
			Assert.AreEqual(SSN_GUNNA, dto.SSN);
			Assert.AreEqual(NAME_GUNNA, dto.Name);

			// Ensure that a new entity object has been created:
			var currentCount = _teacherRegistrations.Count;
			Assert.AreEqual(prevCount + 1, currentCount);

			// Get access to the entity object and assert that
			// the properties have been set:
			var newEntity = _teacherRegistrations.Last();
			Assert.AreEqual(COURSEID_VEFT_20163, newEntity.CourseInstanceID);
			Assert.AreEqual(SSN_GUNNA, newEntity.SSN);
			Assert.AreEqual(TeacherType.MainTeacher, newEntity.Type);

			// Ensure that the Unit Of Work object has been instructed
			// to save the new entity object:
			Assert.IsTrue(_mockUnitOfWork.GetSaveCallCount() > 0);
		}

		[TestMethod]
		[ExpectedException(typeof(AppObjectNotFoundException))]
		public void AddTeacher_InvalidCourse()
		{
			// Arrange:
			var model = new AddTeacherViewModel
			{
				SSN  = SSN_GUNNA,
				Type = TeacherType.AssistantTeacher
			};
			// Note: the method uses test data defined in [TestInitialize]

			// Act:
			_service.AddTeacherToCourse(INVALID_COURSEID, model);
		}

		/// <summary>
		/// Ensure it is not possible to add a person as a teacher
		/// when that person is not registered in the system.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof (AppObjectNotFoundException))]
		public void AddTeacher_InvalidTeacher()
		{
			// Arrange:
			var model = new AddTeacherViewModel
			{
				SSN  = INVALID_SSN,
				Type = TeacherType.MainTeacher
			};
			// Note: the method uses test data defined in [TestInitialize]

			// Act:
			_service.AddTeacherToCourse(COURSEID_VEFT_20153, model);
		}

		/// <summary>
		/// In this test, we test that it is not possible to
		/// add another main teacher to a course, if one is already
		/// defined.
		/// </summary>
		[TestMethod]
		[ExpectedExceptionWithMessage(typeof (AppValidationException), "COURSE_ALREADY_HAS_A_MAIN_TEACHER")]
		public void AddTeacher_AlreadyWithMainTeacher()
		{
			// Arrange:
			var model = new AddTeacherViewModel
			{
				SSN  = SSN_GUNNA,
				Type = TeacherType.MainTeacher
			};
			// Note: the method uses test data defined in [TestInitialize]

			// Act:
			_service.AddTeacherToCourse(COURSEID_VEFT_20153, model);
		}

		/// <summary>
		/// In this test, we ensure that a person cannot be added as a
		/// teacher in a course, if that person is already registered
		/// as a teacher in the given course.
		/// </summary>
		[TestMethod]
		[ExpectedExceptionWithMessage(typeof (AppValidationException), "PERSON_ALREADY_REGISTERED_TEACHER_IN_COURSE")]
		public void AddTeacher_PersonAlreadyRegisteredAsTeacherInCourse()
		{
			// Arrange:
			var model = new AddTeacherViewModel
			{
				SSN  = SSN_DABS,
				Type = TeacherType.AssistantTeacher
			};
			// Note: the method uses test data defined in [TestInitialize]

			// Act:
			_service.AddTeacherToCourse(COURSEID_VEFT_20153, model);
		}

		#endregion
	}
}
