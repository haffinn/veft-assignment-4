﻿namespace CoursesAPI.Services.Utilities
{
	public class DateTimeUtils
	{
        /// <summary>
        /// A boolean returns true if a given year is a leap year
        /// </summary>
        /// <param name="year">Given year</param>
        /// <returns>True if year is leap year, otherwise false</returns>
		public static bool IsLeapYear(int year)
		{
            // Standard rules should be used: 
            // a year is a leap year if it is divisible by 4, 
            // unless it is divisible by 100 (except when it is divisible by 400).
            
		    if (year % 4 == 0) 
            {
                if (year % 100 == 0) 
                {
		            if (year % 400 == 0) 
                    {
		                return true;
		            }
		            return false;
		        }
		        return true;
		    }
			return false;
		}
	}
}
